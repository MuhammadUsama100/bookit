<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');
require "./DBConnect/conn.php";

// $mysql_qry = "SELECT agency.id, agency.name, agency.phone, agency.email, agency.type, agency.address, agency.description, agency.priority, agency.longitude, agency.latitude, agency.profile_image_location, agency.sign_up_date, agency.verified, agency.featured,
// COUNT(user.id) as num_of_agents, COUNT(property.id) as num_of_properties
// FROM agency
// INNER JOIN agent ON agency.id = agent.agency_id
// INNER JOIN user ON agent.user_id = user.id
// INNER JOIN property ON property.added_by = user.id
// GROUP BY agency.id;";
$mysql_qry = "SELECT agency.id, agency.name, agency.phone, agency.email, agency.type, agency.address, agency.description, agency.priority, agency.longitude, agency.latitude, agency.profile_image_location, agency.sign_up_date, agency.verified, agency.featured,
COUNT(user.id) as num_of_agents, COUNT(property.id) as num_of_properties
FROM agency
LEFT JOIN agent ON agency.id = agent.agency_id
LEFT JOIN user ON agent.user_id = user.id
LEFT JOIN property ON property.added_by = user.id
GROUP BY agency.id
ORDER by agency.sign_up_date DESC;";
$result = $conn->query($mysql_qry);

$response = array();
if ($result == TRUE) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($response, array("agency" => $row));
        }
        echo json_encode(array("server_response" => $response));
    }
} else {
    echo json_encode(array("server_response" => -1));            // return -1 means no agency found

}
