import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  Button 
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'

import Login from './Screens/Login'
import SignUp from './Screens/SignUp'
import Welcome from './Screens/Welcome'
import Home from './Screens/Home'
import About from './Screens/About' 
import Booking from './Screens/Booking'
import Profile from './Screens/Profile'
import Start from './Screens/Start'
import Confirm from './Screens/Confirm'

export default class App extends Component {
  render(){
    return (
      <StackAppContaner/>
    );
   }
}

const DrawerNavigator = new createDrawerNavigator ({
  Home : {screen : Home},
  Booking : {screen : Booking},
  //Profile : {screen : Profile}, 
  About : {screen: About}
})

const StackNavigator = new createStackNavigator ({
  Start: {screen : Start},
  Home : {screen : DrawerNavigator},
  Login : {screen : Login},
  SignUp : {screen : SignUp},
  Confirm: {screen : Confirm}
},
{
  headerMode: 'none',
  navigationOptions: {
   // headerVisible: false,
  }
 }
);

const DrawerAppContaner = new createAppContainer (DrawerNavigator)
const StackAppContaner = new createAppContainer (StackNavigator)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
