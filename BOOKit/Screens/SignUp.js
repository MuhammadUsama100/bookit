import React , {Component} from 'react';
import axios from 'axios'
import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  TextInput, 
  Alert,
} from 'react-native';

import { Button } from 'react-native-elements'
import Home from './Home'

class Login extends Component{
    
    constructor(props) {
        super(props);
        this.state = { email_id: '' , first_name: '', last_name: '', username: '', password: ''};
    }

    /*Insertdata = () =>{
        const { email_id } = this.state;
        const { first_name } = this.state;
        const { last_name } = this.state;
        const { username } = this.state;
        const { password } = this.state;

        fetch('http://192.168.15.35/Bookit/DataInput.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email_id: this.state.email_id,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                username: this.state.username,
                password: this.state.password,
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                Alert.alert(responseJson);
            }).catch((error) => {
                console.error(error);
            });
    }*/
    
    Insertdata = () =>{
        const { email_id } = this.state;
        const { first_name } = this.state;
        const { last_name } = this.state;
        const { username } = this.state;
        const { password } = this.state;

        // fetch('http://192.168.15.35/Bookit/DataInput.php?email_id='+email_id+'&first_name='+first_name+'&last_name='+last_name+'&username='+username+'&password='+password).then((response) => response.json())
        //     .then((responseJson) => {
        //         Alert.alert(responseJson);
        //     }).catch((error) => {
        //         console.error(error);
        //     });

        link="http://192.168.0.103/Bookit/DataInput.php?email_id="+email_id+'&first_name='+first_name+'&last_name='+last_name+'&username='+username+'&password='+password
         console.log(link)
         axios.get(link).then((result)=>{
             console.log(result.data)
             //this.props.navigation.navigate('Home')
         })

    }

    
    
    render(){
        return(
            <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TextInput
                    editable = {true}
                    placeholder = 'Email'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15
                        }}
                    onChangeText={email_id=> this.setState({ email_id })}
                    value={this.state.email_id}
                />
                <TextInput
                    editable = {true}
                    placeholder = 'First Name'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15,
                        marginTop: 20
                        }}
                    onChangeText={first_name => this.setState({ first_name })}
                    value={this.state.first_name}
                />
                <TextInput
                    editable = {true}
                    placeholder = 'Last Name'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15,
                        marginTop: 20
                        }}
                    onChangeText={last_name => this.setState({ last_name })}
                    value={this.state.last_name}
                />
                <TextInput
                    editable = {true}
                    placeholder = 'Set Username'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15,
                        marginTop: 20
                        }}
                    onChangeText={username => this.setState({ username })}
                    value={this.state.username}
                />
                <TextInput
                    editable = {true}
                    secureTextEntry = {true}
                    placeholder = 'Set Password'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15,
                        marginTop: 20
                        }}
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}
                />
                <Button
                    buttonStyle = {{marginTop: 20, backgroundColor: 'lightskyblue', width: 100}}
                    title = 'Register'
                    type = 'solid' 
                    onPress = {this.Insertdata}/>
            </View>

        );
    }
}

export default Login