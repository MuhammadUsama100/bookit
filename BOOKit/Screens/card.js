import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import axios from 'axios';

class EventCard extends Component {


  constructor(props){
    super(props)
    this.state = {
        posts : [],
    }
  }

  
    render() {

      const { posts } = this.state;
        return( 
    
          <View style={styles.container}>
            {/* <TouchableOpacity style={styles.container}> */}
              
              <View style={styles.card}>
                  <Text style={styles.cardTitle}>HTMLCSS WORKSHOP</Text>
                  <Text style={styles.cardOrganizer}>TWM</Text>
                  <Text style={styles.cardVenue}>Lab 09</Text>
                  <Text style={styles.cardLink}>https://mail.google.com/mail/u/0/#inbox</Text>
              </View>
            {/* </TouchableOpacity> */}
          </View>
                
        );

    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop:20,
      backgroundColor: '#F7F7F7',
      flexDirection: "row"
    },
    timecard:{
      flex: 3,
      backgroundColor: '#F7F7F7',
      alignItems: 'center', 
      justifyContent: 'center',
      //height: '30%',
      
    },
    eventTime:{
      alignContent: "center",
      transform: [{ rotate: '-90deg'}]
    },
    card: {
      flex: 6,
      backgroundColor:'#DF3F7C',
      borderRadius: 20,
      marginBottom: 10,
      width:'90%',
      height: '30%',
      shadowColor: '#fff',
      alignContent: "space-between",
      shadowOpacity: 0.8,
      shadowRadius: 1,
      shadowOffset:{
        width:3,
        height:3
      },
      
    },
    cardTitle: {
      color: '#fff',
      fontSize:22,
      fontWeight:"bold",
      marginTop: 10,
      textAlign: "left",
      marginLeft: 10,
    },
    cardOrganizer: {
      color: '#fff',
      fontSize: 20,
      fontWeight:"bold",
      marginLeft: 10,
    },
    cardVenue: {
      color: '#fff',
      fontSize:16,
      marginLeft: 10,
      marginBottom: 3,      
    },
    cardLink: {
      color: '#fff',
      fontSize:12,
      marginBottom: 10,
      marginLeft: 10,
    },
    
});

export default EventCard;