import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Picker 
} from 'react-native';

import {
    Container,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Card,
    CardItem
} from 'native-base'

import { Button } from 'react-native-elements' 
import Confirm from './Confirm'
import { NavigationEvents } from 'react-navigation';


class Booking extends Component {

    constructor(props){
        super(props)
        this.state = { city: '' ,lower_price: '' ,upper_price: '' ,rating: '' }
    }
    updatecity = (city) => {
        this.setState({city: city})
    }
    updatelowerprice = (lower_price) => {
        this.setState({lower_price: lower_price})
    }
    updateupperprice = (upper_price) => {
        this.setState({upper_price: upper_price})
    }
    updaterating = (rating) => {
        this.setState({rating: rating})
    }

    render(){

        const { navigate } = this.props.navigation

        return(
            <View >
                <View 
                style = {{
                    flexDirection: 'row', 
                    height: 100, 
                    alignItems: 'center',
                    paddingHorizontal:'5%',
                    justifyContent: 'flex-start'
                   }}>
           
                    <Icon onPress = {this.props.navigation.openDrawer}
                        name = "md-menu" size = {100} style = {{color: 'black',}}/>    
                </View>
                <View style = {{flex: 1, alignItems: 'center' , justifyContent: 'center', paddingHorizontal: 20, marginTop: 10}}>
                    <Text style = {{fontSize: 24, fontWeight: 'bold', color: 'gray'}}>Search according to your requirements</Text>
                </View>
                <View style={{ 
                        height: 50, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 20,
                        borderRadius: 15, marginTop: 80,
                        marginLeft: 10, justifyContent: 'center'
                        }}>
                    <Picker style = {{ 
                        width: '100%'
                    }} selectedValue = {this.state.city} onValueChange = {this.updatecity}>
                    <Picker.Item label = 'Select City' value = '0'/>
                    <Picker.Item label = 'Karachi' value = 'Karachi'/>
                    <Picker.Item label = 'Lahore' value = 'Lahore'/>
                    <Picker.Item label = 'Islamabad' value = 'Islamabad'/>
                    <Picker.Item label = 'Peshawar' value = 'Peshawar'/>   
                    </Picker>   
                </View>

                <View style={{ 
                        height: 50, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 20,
                        borderRadius: 15, marginTop: 40,
                        marginLeft: 10, justifyContent: 'center'
                        }}>
                    <Picker style = {{ 
                        width: '100%'
                    }} selectedValue = {this.state.lower_price} onValueChange = {this.updatelowerprice}>
                    <Picker.Item label = 'Select Your Starting Price' value = '-1'/>
                    <Picker.Item label = '0' value = '0'/>
                    <Picker.Item label = '10000' value = '10000'/>
                    <Picker.Item label = '20000' value = '20000'/>
                    <Picker.Item label = '30000' value = '30000'/>   
                    </Picker>   
                </View>

                <View style={{ 
                        height: 50, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 20,
                        borderRadius: 15, marginTop: 40,
                        marginLeft: 10, justifyContent: 'center'
                        }}>
                    <Picker style = {{ 
                        width: '100%'
                    }} selectedValue = {this.state.upper_price} onValueChange = {this.updateupperprice}>
                    <Picker.Item label = 'Select Your Ending Price' value = '0'/>
                    <Picker.Item label = '10000' value = '10000'/>
                    <Picker.Item label = '20000' value = '20000'/>
                    <Picker.Item label = '30000' value = '30000'/>
                    <Picker.Item label = '40000' value = '40000'/>   
                    </Picker>   
                </View>    

                <View style={{ 
                        height: 50, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 20,
                        borderRadius: 15, marginTop: 40,
                        marginLeft: 10, justifyContent: 'center'
                        }}>
                    <Picker style = {{ 
                        width: '100%'
                    }} selectedValue = {this.state.rating} onValueChange = {this.updaterating}>
                    <Picker.Item label = 'Select rating' value = '0'/>
                    <Picker.Item label = '1' value = '1'/>
                    <Picker.Item label = '2' value = '2'/>
                    <Picker.Item label = '3' value = '3'/>
                    <Picker.Item label = '4' value = '4'/>
                    <Picker.Item label = '5' value = '5'/>   
                    </Picker>   
                </View>
                
                <View style = {{alignItems: 'center', marginTop: 50}}>
                <Button
                    buttonStyle = {{backgroundColor: 'lightskyblue', width: 100}}
                    title = 'Search'
                    type = 'solid' 
                    onPress = {() => this.props.navigation.navigate('Confirm', {
                        city: this.state.city,
                        lower_price: this.state.lower_price,
                        upper_price: this.state.upper_price,
                        rating: this.state.rating
                    })}
                    />
                </View>
            </View>
        );

    }
}

export default Booking