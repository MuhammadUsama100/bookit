import React , {Component} from 'react';

import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'

import {
    Container,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Card,
    CardItem,
    Row
} from 'native-base'

import Room1 from '../Cards/Room1'
import Room2 from '../Cards/Room2'
import Room3 from '../Cards/Room3'
import { Button } from 'react-native-elements'
import Swiper from 'react-native-swiper'
import Login from './Login'
import SignUp from './SignUp'
import Start from './Start'

class Home extends Component {
    render(){
        return(
            <ScrollView showsVerticalScrollIndicator={false}>
                <View 
                    style = {{
                        flexDirection: 'row', 
                        height: 100, 
                        alignItems: 'center',
                        paddingHorizontal:'5%',
                        justifyContent: 'space-between'
                        }}>
           
                        <Icon onPress = {this.props.navigation.openDrawer}
                            name = "md-menu" size = {100} style = {{color: 'black',}}/>
                        <Image style = {{height: 120, width: 500, marginTop: 35}}
                            source = {require('../assets/logo.png')}/>    
                </View>
                <View style = {{flexDirection: 'row', paddingHorizontal: 10, height: 30}}>
                    <Button 
                        title = 'Log Out'
                        type = 'outline' 
                        onPress = {() => this.props.navigation.navigate('Start')}/>
                </View>
                <View style = {{height: 50, paddingHorizontal: 10, justifyContent: 'center'}}>
                    <Text style = {{fontWeight: 'bold', fontSize: 20, color: 'dimgray'}}>A LOOK AT WHAT WE SERVE</Text>
                </View>
                <View style = {{height: 170}}>
                    <Swiper
                        autoplay = {true} 
                        style = {{height: 100}}>
                        <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Image style = {{height: 130, width: 360}}
                            source = {require('../assets/pc.png')}/>
                        </View>
                        <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Image style = {{height: 130, width: 360}}
                            source = {require('../assets/avari.png')}/>
                        </View>
                        <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Image style = {{height: 130, width: 360}}
                            source = {require('../assets/marriot.png')}/>
                        </View>
                    </Swiper>
                </View>
                <View style = {{height: 50, paddingHorizontal: 10, justifyContent: 'center'}}>
                    <Text style = {{fontWeight: 'bold', fontSize: 20, color: 'dimgrey'}}>
                        MOST POPULAR 
                    </Text>
                </View>
                <Card style = {{marginLeft: 5, marginRight: 5}}>
                    <Room1/>
                    <Room2/>
                    <Room3/>
                </Card>
            </ScrollView>
        );
    }
}

export default Home