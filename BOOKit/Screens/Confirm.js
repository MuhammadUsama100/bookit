import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  TextInput,
  Picker,
  FlatList,
  SafeAreaView 
} from 'react-native';

import axios from 'axios'
import { Button } from 'react-native-elements'

class Confirm extends Component{

    constructor(props){
        super(props)
        this.state = {
            // options:[],
                        DATA: [],
                        city: this.props.navigation.getParam('city',''),
                        lower_price: this.props.navigation.getParam('lower_price',''),
                        upper_price: this.props.navigation.getParam('upper_price',''),
                        rating: this.props.navigation.getParam('rating',''),
                        day: '', mmonth: ''
                    }

    }
    ListView = () =>{
        const { city } = this.state;
        const { lower_price } = this.state;
        const { upper_price } = this.state;
        const { rating } = this.state;
        const { response } = this.state;
        // link = "http://localhost/Bookit/select.php?city="+city+'&lower_price='+lower_price+'&upper_price='+upper_price+'&rating='+rating 
        link = 'http://localhost/Bookit/select.php?city=Karachi&lower_price=0&upper_price=40000&rating=4'
        console.log(link)
         axios.get(link).then((result)=>{
             console.log(result.data);
             this.setState({
                DATA: result.data
             });
             //navigate to Home
             //this.props.navigation.navigate('Home')
         })
    }
    updateday = (day) => {
        this.setState({day: day})
    }

    select = () => {
        const { city } = this.state;
        const { lower_price } = this.state;
        const { upper_price } = this.state;
        const { rating } = this.state;

        link = "http://localhost/Bookit/select.php?city="+city+'&lower_price='+lower_price+'&upper_price='+upper_price+'&rating='+rating 
        console.log(link)
         axios.get(link).then((result)=>{
             console.log(result.data)
            
             //navigate to Home
             //this.props.navigation.navigate('Home')
         })

    }

    Item = ({ title }) => {
        return (
          <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
          </View>
        );
      }
      

  
    render(){
        const { navigation } = this.props;
        //const { city } = this.state.city;
        //const { price } = this.state.price;
        //const { rating } = this.state.rating;

        return(
            <View style = {{alignItems: 'center'}}>

                <View style={{
                        flexDirection: 'row', 
                        height: 50, borderColor: 'gray',
                        width: 60, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 20,
                        borderRadius: 15, marginTop: 80,
                        justifyContent: 'center'
                        }}>
                    <Picker style = {{ 
                        width: '100%'
                    }} selectedValue = {this.state.day} 
                    onValueChange={()=>{}}>
                    {/* {Object.keys(options).map((key) => {
                        return (<Picker.Item label={this.props.options[key]} value={key} key={key}/>) //if you have a bunch of keys value pair
                    })} */}
                </Picker>
                </View>

                <Button
                    buttonStyle = {{marginTop: 20, backgroundColor: 'lightskyblue', width: 100}}
                    title = 'confirm'
                    type = 'solid' 
                    onPress = {this.ListView}
                    />
                
                
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 20,
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
  });
  

export default Confirm
