import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  TextInput,
  Image 
} from 'react-native';

import {
    Container,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Card,
    CardItem
} from 'native-base'


class Profile extends Component {
    render(){
        return(
            <View>
                <View 
                style = {{
                    flexDirection: 'row', 
                    height: 100, 
                    alignItems: 'center',
                    paddingHorizontal:'5%',
                    justifyContent: 'flex-start'
                   }}>
           
                    <Icon onPress = {this.props.navigation.openDrawer}
                        name = "md-menu" size = {100} style = {{color: 'black',}}/>    
                </View>
                <View style = {{flex: 1, alignItems: 'center' , justifyContent: 'center'}}>
                    <Text>Profile page under construction</Text>
                </View>
            </View>
        );

    }
}

export default Profile