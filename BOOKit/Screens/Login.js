import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  TextInput 
} from 'react-native';

import axios from 'axios'
import { Button } from 'react-native-elements'
import Home from './Home'
import { NavigationEvents } from 'react-navigation';

class Login extends Component{
    
    constructor(props) {
        super(props);
        this.state = { username: '' , password: ''};
      }
    
    userauth = () => {
        /*const { username } = this.state;
        const { password } = this.state;

        link = "http://192.168.0.103/Bookit/login.php?username="+username+'&password='+password
        console.log(link)
         axios.get(link).then((result)=>{
             console.log(result.data)
             //navigate to Home
             this.props.navigation.navigate('Home')
         })*/
         this.props.navigation.navigate('Home')
    } 

    
    render(){
        return(
            <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TextInput
                    editable = {true}
                    placeholder = 'Username'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15
                        }}
                    onChangeText={username => this.setState({ username })}
                    value={this.state.username}
                />
                <TextInput
                    editable = {true}
                    secureTextEntry = {true}
                    placeholder = 'Password'
                    placeholderTextColor = 'gainsboro'
                    style={{ 
                        height: 40, borderColor: 'gray',
                        width: 340, borderWidth: 0.5, 
                        borderRightColor: 'grey',
                        paddingHorizontal: 5,
                        borderRadius: 15,
                        marginTop: 20
                        }}
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}
                />
                
                <Button
                    buttonStyle = {{marginTop: 20, backgroundColor: 'lightskyblue', width: 100}}
                    title = 'Login'
                    type = 'solid' 
                    onPress = {this.userauth}/>
            </View>

        );
    }
}

export default Login