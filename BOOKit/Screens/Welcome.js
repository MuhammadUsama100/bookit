import React , {Component} from 'react';

import 
{ 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  Button 
} from 'react-native';

import Login from './Login'
import SignUp from './SignUp'

class Welcome extends Component{
    render(){
        return(
            <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Button title = 'Login' onPress = {() => this.props.navigation.navigate('Login')}/>
                <Button title = 'Sign Up' onPress = {() => this.props.navigation.navigate('SignUp')}/>         
            </View>

        );
    }
}

export default Welcome