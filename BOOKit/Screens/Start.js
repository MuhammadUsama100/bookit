import React , {Component} from 'react';

import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'

import {
    Container,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Card,
    CardItem
} from 'native-base'

import { Button } from 'react-native-elements'
import Login from './Login'
import SignUp from './SignUp'

class Start extends Component{

    render(){
        return(
            <View 
                style = {{
                    flex: 1,
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    justifyContent: 'center'
                    }}>
                <Button
                    buttonStyle = {{marginTop: 20, width: 160, borderRadius: 20}} 
                    title = 'Login'
                    type = 'outline'
                    onPress = {() => this.props.navigation.navigate('Login')}/>
                <Text style = {{fontSize: 30}}>  </Text>    
                <Button
                    buttonStyle = {{marginTop: 20, width: 160, borderRadius: 20}} 
                    title = 'Sign Up'
                    type = 'outline' 
                    onPress = {() => this.props.navigation.navigate('SignUp')}/>
            </View>

        );
    }
}

export default Start