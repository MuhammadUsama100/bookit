import React , {Component} from 'react'
import{
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native'

import{
    Card,
    CardItem,
    Right,
} from 'native-base'

import StarRating from 'react-native-star-rating'

class Room3 extends Component {
    render() {
        return(
            <CardItem>
                <View>
                    <Image style = {{height: 90, width: 80}}
                    source = {require('../assets/rom3.png')}/>
                </View>
                <Right style = {{flex: 1, alignItems: 'flex-start', height: 90, paddingHorizontal: 20}}>
                    <Text>Economy Multi room</Text>
                    <Text style = {{color: 'grey', fontSize: 11}}>Sunset Suites</Text>
                    <Text style = {{fontSize: 14, fontWeight: 'bold', color: '#c4402f'}}>Rs.11000</Text>
                   <StarRating
                        disabled = {true}
                        maxStars = {5}
                        rating = {3}
                        starSize = {12}
                        fullStarColor = 'orange'
                        emptyStarColor = 'orange'
                    />
                </Right>
            </CardItem>
        );
    }
}

export default Room3