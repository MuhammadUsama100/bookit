import React , {Component} from 'react'
import{
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native'

import{
    Card,
    CardItem,
    Right,
} from 'native-base'

import StarRating from 'react-native-star-rating'

class Room2 extends Component {
    render() {
        return(
            <CardItem>
                <View>
                    <Image style = {{height: 90, width: 80}}
                    source = {require('../assets/room2.png')}/>
                </View>
                <Right style = {{flex: 1, alignItems: 'flex-start', height: 90, paddingHorizontal: 20}}>
                    <Text>Room for 1</Text>
                    <Text style = {{color: 'grey', fontSize: 11}}>Days Inn hotel</Text>
                    <Text style = {{fontSize: 14, fontWeight: 'bold', color: '#c4402f'}}>Rs.16000</Text>
                   <StarRating
                        disabled = {true}
                        maxStars = {5}
                        rating = {3.5}
                        starSize = {12}
                        fullStarColor = 'orange'
                        emptyStarColor = 'orange'
                    />
                </Right>
            </CardItem>
        );
    }
}

export default Room2